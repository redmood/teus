export default class TEUSItemSheet extends ItemSheet {
  get template() {
    return `systems/teus/templates/items/${this.item.type}-sheet.html`;
  }

  getData() {
    console.log('TEUS | ItemSheet');
    const context = super.getData();

    context.config = CONFIG.TEUS;
    context.actor = this.object.actor;
    context.system = this.object.system;

    return context;
  }
}
