export default class TEUSPCStudentSheet extends ActorSheet {
  /** @override */
  get template() {
    return `systems/teus/templates/actors/${this.actor.type}-sheet.html`;
  }

  /** @override */
  getData() {
    const context = super.getData();
    const actorData = this.actor.toObject(false);

    context.system = actorData.system;
    context.config = CONFIG.TEUS;
    this._prepareCharacterData(context);
    context.rollData = context.actor.getRollData();

    return context;
  }

   /**
    * Organize and classify Items for Character sheets.
    *
    * @param {Object} actorData The actor to prepare.
    *
    * @return {undefined}
    */
   _prepareCharacterData(context) {
     // Extract and seperates items types
     context.items.forEach((item) => {
       const itemType = item.type;
       // building context property name from itemType
       const parts = itemType.split('-');
       let propName = parts[0];

       parts.slice(1).forEach(part => {
         propName += part[0].toUpperCase() + part.slice(1);
       });

       propName = `${propName}s`;

       if (context.hasOwnProperty(`${propName}`)) {
         context[propName].push(item);
       } else {
         context[propName] = [item];
       };
     });

     // Computed current spent eveil points
     let curEveil = 0;

     if (context.hasOwnProperty('features')) {
       context.features.forEach((feature) => {
         curEveil += feature.system.price;
       });
     };

     context.system.eveil = curEveil;
   }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Rollable abilities.
    html.find('.rollable').click(this._onRoll.bind(this));

    // Create, Edit, Delete items
    html.find('.item-create').click(this._onItemCreate.bind(this));
    html.find('.item-open').click(this._onItemOpen.bind(this));
    html.find('.inline-edit').change(this._onItemEdit.bind(this));
    html.find('.item-delete').click(this._onItemDelete.bind(this));
  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  async _onRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    // Handle rolls that supply the formula directly.
    let label = dataset.label ? `${dataset.label}` : '';

    if (dataset.type==='attribute') {
      var roll_data = this.getAttributeRoll(dataset.name);
    } else if (dataset.type==='magic') {
      var roll_data = 'd20+@magic';
    } else if (dataset.type==='skill') {
      let attribute = await this._getSkillCheckOption(dataset.parents);
      var roll_data = this.getSkillRoll(attribute, dataset.name);
    }

    let roll = new Roll(roll_data, this.actor.getRollData());
    roll.toMessage({
      speaker: ChatMessage.getSpeaker({ actor: this.actor }),
      flavor: label,
      rollMode: game.settings.get('core', 'rollMode'),
    });
    return roll;
  }

  async _getSkillCheckOption(options) {
    const template = "systems/teus/templates/dialogs/skill-check-dialog.html";
    let options_set = Array();
    options.split(',').forEach(option => {
        options_set.push({value: option, text: `teus.attributes.${option}`});
    });
    const html = await renderTemplate(template, {options: options_set});

    return new Promise(resolve => {
      const data = {
        title: game.i18n.format("teus.rolls.skillcheck"),
        content: html,
        buttons: {
          normal: {
            label: game.i18n.format("teus.rolls.do"),
            callback: async html => resolve(this._processSkillCheckOptions(html.find("#parent")[0]))
          },
          cancel: {
            label: game.i18n.format("teus.rolls.cancel"),
            callback: () => {cancelled: true}
          }
        },
        default: "normal",
        close: () => {cancelled: true}
      };

      new Dialog(data, null).render(true);
    });
  }

  _processSkillCheckOptions(select) {
    let selectedIndex = select.options.selectedIndex;
    return select.options[selectedIndex].value;
  }

  async _onItemCreate(event) {
    event.preventDefault();
    // extracting origin element to get new item info from
    let element = event.currentTarget;
    // extracting new item type
    let item_type = element.dataset.type;

    // prepare new item object
    let itemData = {
      name: `New ${item_type.capitalize()}`,
      type: item_type
    };

    // creating item for current actor
    return await Item.create(itemData, {parent: this.actor});
  }

  _onItemEdit(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const tr = $(element).parents('.item-id')[0];
    const item = this.actor.items.get(tr.dataset['itemId']);
    const field = element.dataset.field;
    return item.update({ [field]: element.value });
  }

  _onItemOpen(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const tr = $(element).parents('.item-id')[0];
    const item = this.actor.items.get(tr.dataset['itemId']);
    item.sheet.render(true);
  }

  _onItemDelete(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const tr = $(element).parents('.item-id')[0];
    const item = this.actor.items.get(tr.dataset['itemId']);
    item.delete();
  }

  getAttributeRoll(rollName) {
    switch(rollName) {
      case 'body':
        return 'd20+@body+@energy';
      case 'spirit':
        return 'd20+@spirit+@erudition';
      case 'heart':
        return 'd20+@heart+@friends';
      default:
        console.log(`Bad roll name : ${rollName}.`);
    }
  }

  getSkillRoll(attribute, skill) {
    return `d20+@${attribute}+@${skill}`;
  }
}
