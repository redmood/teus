export const TEUS = {};

TEUS.commons = {
  "name": "teus.commons.name",
  "description": "teus.commons.description",
  "effect": "teus.commons.effect",
  "level": "teus.commons.level",
  "mastered": "teus.commons.mastered"
};

TEUS.identity = {
  "age": "teus.identity.age",
  "blood": "teus.identity.blood",
  "richness": "teus.identity.richness",
  "ambition": "teus.identity.ambition",
  "birthdate": "teus.identity.birthdate",
  "birthplace": "teus.identity.birthplace"
};

TEUS.scolarity = {
  "school": "teus.scolarity.school",
  "year": "teus.scolarity.year",
  "eveil": "teus.scolarity.eveil",
};

TEUS.wand = {
  "wood": "teus.wand.wood",
  "length": "teus.wand.length",
  "wheart": "teus.wand.wheart"
};

TEUS.magic = "teus.magic";

TEUS.attributes = {
  "body": "teus.attributes.body",
  "spirit": "teus.attributes.spirit",
  "heart": "teus.attributes.heart"
};

TEUS.capacities = {
  "energy": {"text": "teus.capacities.energy", "parentAttribute": "body"},
  "erudition": {"text": "teus.capacities.erudition", "parentAttribute": "spirit"},
  "friends": {"text": "teus.capacities.friends", "parentAttribute": "heart"}
};

TEUS.skills = {
  "fight": {"text": "teus.skills.fight", "parentAttributes": ["body", "spirit"]},
  "stamina": {"text": "teus.skills.stamina", "parentAttributes": ["body", "spirit"]},
  "precision": {"text": "teus.skills.precision", "parentAttributes": ["body", "spirit"]},
  "perception": {"text": "teus.skills.perception", "parentAttributes": ["body", "spirit"]},
  "decorum": {"text": "teus.skills.decorum", "parentAttributes": ["body", "heart"]},
  "stealth": {"text": "teus.skills.stealth", "parentAttributes": ["body", "heart"]},
  "persuasion": {"text": "teus.skills.persuasion", "parentAttributes": ["body", "heart"]},
  "relational": {"text": "teus.skills.relational", "parentAttributes": ["body", "heart"]},
  "bluff": {"text": "teus.skills.bluff", "parentAttributes": ["heart", "spirit"]},
  "farce": {"text": "teus.skills.farce", "parentAttributes": ["heart", "spirit"]},
  "rumor": {"text": "teus.skills.rumor", "parentAttributes": ["heart", "spirit"]},
  "tactic": {"text": "teus.skills.tactic", "parentAttributes": ["heart", "spirit"]}
};

TEUS.features = {
  "add": "teus.features.add",
  "price": "teus.features.price"
};

TEUS.subjects = {
  "add": "teus.subjects.add",
  "level": "teus.subjects.level",
  "bonus": "teus.subjects.bonus"
}

TEUS.spells = {
  "add": "teus.spells.add",
  "formula": "teus.spells.formula",
  "type": "teus.spells.type",
  "targets": "teus.spells.targets",
  "favorite": "teus.spells.favorite",
  "legend": "teus.spells.legend",
  "legendlabel": "teus.spells.legendlabel"
}

TEUS.potions = {
  "add": "teus.potions.add",
}

TEUS.choices = {
  "unknown": "teus.choices.unknown",
  "bloods": {
    "muggleborn": "teus.choices.bloods.muggleborn",
    "halfblood": "teus.choices.bloods.halfblood",
    "pureblood": "teus.choices.bloods.pureblood"
  },
  "spelltypes": {
    "charm": "teus.choices.spelltypes.charm",
    "transfiguration": "teus.choices.spelltypes.transfiguration",
    "defense": "teus.choices.spelltypes.defense",
    "jinx": "teus.choices.spelltype.jinx",
    "hex": "teus.choices.spelltype.hex",
    "curse": "teus.choices.spelltypes.curse"
  },
  "subjectlevels": {
    "troll": "teus.choices.subjectlevels.troll",
    "dreadful": "teus.choices.subjectlevels.dreadful",
    "poor": "teus.choices.subjectlevels.poor",
    "average": "teus.choices.subjectlevels.average",
    "good": "teus.choices.subjectlevels.good",
    "excellent": "teus.choices.subjectlevels.excellent",
    "genius": "teus.choices.subjectlevels.genius"
  },
  "consequencestypes": {
    "minor": "teus.choices.consequencestypes.minor",
    "major": "teus.choices.consequencestypes.major"
  }
}

TEUS.rolls = {
  "skillcheck": "teus.rolls.skillcheck",
  "do": "teus.rolls.do",
  "cancel": "teus.rolls.cancel",
  "with": "teus.rolls.with"
}

TEUS.tabs = {
  "caracs": "teus.tabs.caracs",
  "features": "teus.tabs.features",
  "subjects": "teus.tabs.subjects",
  "spells": "teus.tabs.spells",
  "potions": "teus.tabs.potions",
  "inventory": "teus.tabs.inventory",
  "consequences": "teus.tabs.consequences"
}

TEUS.sections = {
  "wand": "teus.sections.wand",
  "scolarity": "teus.sections.scolarity"
}

TEUS.inventory = {
  "add": "teus.inventory.add",
  "quantity": "teus.inventory.quantity"
}

TEUS.shools = {
  "add": "teus.schools.add",
  "name": "teus.schools.name",
  "short_description": "teus.schools.short_description",
  "long_description": "teus.schools.long_description",
  "year": "teus.schools.year"
}

TEUS.consequences = {
  "add": "teus.consequences.add",
  "importance": "teus.consequences.importance",
  "permanent": "teus.consequences.permanent"
}

TEUS.friendcards = {
  "add": "teus.friendcards.add",
  "traits": "teus.friendcards.traits",
  "special": "teus.friendcards.special"
}

TEUS.wands = {
  "add": "teus.wands.add",
  "wood": "teus.wands.wood",
  "length": "teus.wands.length",
  "heart": "teus.wands.heart",
  "flexibility": "teus.wands.flexibility"
}