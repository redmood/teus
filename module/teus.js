import {TEUS} from "./config.js";
import TEUSPCStudentSheet from "./actors/TEUSPCStudentSheet.js";
import TEUSItemSheet from "./items/TEUSItemSheet.js";

async function preloadHandlebarsTemplates() {
  const templatePaths = [
    "systems/teus/templates/partials/student-features-block.html",
    "systems/teus/templates/partials/student-spells-block.html",
    "systems/teus/templates/partials/student-potions-block.html",
    "systems/teus/templates/partials/student-subjects-block.html",
    "systems/teus/templates/partials/student-inventory-block.html",
    "systems/teus/templates/partials/student-schools-block.html",
    "systems/teus/templates/partials/student-friendcards-block.html",
    "systems/teus/templates/partials/student-wands-block.html"
  ];

  return loadTemplates(templatePaths);
};

Hooks.once("init", function() {
  console.log("TEUS | Initializing Tu Es Un Sorcier");

  // Add utility classes to the global game object so that they're more easily
  // accessible in global contexts.
  game.teus = {
    TEUSPCStudentSheet,
    TEUSItemSheet,
  };

  // Add custom constants for configuration
  CONFIG.TEUS = TEUS;

  // Define custom Document classes
//  CONFIG.Actor.documentClass = TEUSPCStudentSheet;
//  CONFIG.Item.documentClass = TEUSItemSheet;

  // Register sheet application classes
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("teus", TEUSItemSheet, { makeDefault: true });
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("teus", TEUSPCStudentSheet, { makeDefault: true });

  preloadHandlebarsTemplates();
});
