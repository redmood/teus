I (redmood) own this repository and code, but I do not have any rights on the original "Tu Es Un Sorcier" roleplay game system. For more information about the game system see [the creators blog](https://jeudijdr.blogspot.com/p/tu-es-un-sorcier.html) and [the system’s rulebook](https://drive.google.com/file/d/1WzJ-mWOCTQG96Mdw4ludjpCKIfLShkDM/view).

This repository is under license CC-BY-SA 4.0